﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CsvHelper;
using StrongTypes;
using MectricsCSharp;
using g3;

namespace TrackingEvaluation
{
    public static class WriterToFiles
    {
        private static string writePath = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName) + @"\Output";
        private static string csvPath = writePath + @"\Metrics.csv";
        private static string distSixNodesHtcPath = writePath + @"\DistanceSixNodesHtc.txt";
        private static string distAnnSixNodesPath = writePath + @"\DistanceAnnSixNodes.txt";
        private static string deflSixNodesHtcPath = writePath + @"\DeflectionSixNodesHtc.txt";
        private static string deflAnnSixNodesPath = writePath + @"\DeflectionAnnSixNodes.txt";

        public static void WriteStatsToCsv()
        {
            using (StreamWriter sw = new StreamWriter(csvPath, false, Encoding.Default))
            {
                using (CsvWriter csvWriter = new CsvWriter(sw))
                {
                    if (!Directory.Exists(writePath))
                        Directory.CreateDirectory(writePath);

                    csvWriter.WriteField("Метрика Distance -- среднее расстояние между контроллерами Finch (Six Nodes и ANN) и HTC в см");
                    csvWriter.NextRecord();
                    csvWriter.WriteField("Движение");
                    csvWriter.WriteField("Среднее между Finch Six Nodes и Htc");
                    csvWriter.WriteField("Среднее между Finch ANN и Finch Six Nodes");
                    csvWriter.NextRecord();

                    for (int i = 0; i < RecordStats.distanceSixNodesAndHtc.Count; i++)
                    {
                        csvWriter.WriteField(DataManager_v06.FileDatas[i].name);
                        csvWriter.WriteField(((Centimeters)RecordStats.distanceSixNodesAndHtc[i].Average).ToString("f3"));
                        csvWriter.WriteField(((Centimeters)RecordStats.distanceAnnAndSixNodes[i].Average).ToString("f3"));
                        csvWriter.NextRecord();
                    }

                    csvWriter.NextRecord();

                    csvWriter.WriteField("Min");
                    Meters min = new Meters(RecordStats.finalStatsDistanceSixNodesAndHtc.Min);
                    csvWriter.WriteField(((Centimeters)min).ToString("f3"));
                    min = new Meters(RecordStats.finalStatsDistanceAnnAndSixNodes.Min);
                    csvWriter.WriteField(((Centimeters)min).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.WriteField("Max");
                    Meters max = new Meters(RecordStats.finalStatsDistanceSixNodesAndHtc.Max);
                    csvWriter.WriteField(((Centimeters)max).ToString("f3"));
                    max = new Meters(RecordStats.finalStatsDistanceAnnAndSixNodes.Max);
                    csvWriter.WriteField(((Centimeters)max).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.WriteField("Average");
                    Meters avg = new Meters(RecordStats.finalStatsDistanceSixNodesAndHtc.Average);
                    csvWriter.WriteField(((Centimeters)avg).ToString("f3"));
                    avg = new Meters(RecordStats.finalStatsDistanceAnnAndSixNodes.Average);
                    csvWriter.WriteField(((Centimeters)avg).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.WriteField("Median");
                    Meters med = new Meters(RecordStats.finalStatsDistanceSixNodesAndHtc.Median);
                    csvWriter.WriteField(((Centimeters)med).ToString("f3"));
                    med = new Meters(RecordStats.finalStatsDistanceAnnAndSixNodes.Median);
                    csvWriter.WriteField(((Centimeters)med).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.WriteField("Percentil 99%");
                    Meters prcnt = new Meters(RecordStats.finalStatsDistanceSixNodesAndHtc.Percentil);
                    csvWriter.WriteField(((Centimeters)prcnt).ToString("f3"));
                    prcnt = new Meters(RecordStats.finalStatsDistanceAnnAndSixNodes.Percentil);
                    csvWriter.WriteField(((Centimeters)prcnt).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.NextRecord();

                    csvWriter.WriteField("Метрика Deflection Angle -- угол между направлениями" +
                        " костей Lower Arm (Finch(Six Nodes), Finch ANN и HTC) в градусах");
                    csvWriter.NextRecord();
                    csvWriter.WriteField("Движение");
                    csvWriter.WriteField("Среднее между Finch Six Nodes и HTC");
                    csvWriter.WriteField("Среднее между Finch ANN и Finch Six Nodes");
                    csvWriter.NextRecord();

                    for (int i = 0; i < RecordStats.deflectionsSixNodesAndHtc.Count; i++)
                    {
                        csvWriter.WriteField(DataManager_v06.FileDatas[i].name);
                        csvWriter.WriteField(((Degrees)RecordStats.deflectionsSixNodesAndHtc[i].Average).ToString("f3"));
                        csvWriter.WriteField(((Degrees)RecordStats.deflectionsAnnAndSixNodes[i].Average).ToString("f3"));
                        csvWriter.NextRecord();
                    }

                    csvWriter.NextRecord();

                    csvWriter.WriteField("Min");
                    Radians minAngle = new Radians(RecordStats.finalStatsDeflectionSixNodesAndHtc.Min);
                    csvWriter.WriteField(((Degrees)minAngle).ToString("f3"));
                    minAngle = new Radians(RecordStats.finalStatsDeflectionAnnAndSixNodes.Min);
                    csvWriter.WriteField(((Degrees)minAngle).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.WriteField("Max");
                    Radians maxAngle = new Radians(RecordStats.finalStatsDeflectionSixNodesAndHtc.Max);
                    csvWriter.WriteField(((Degrees)maxAngle).ToString("f3"));
                    maxAngle = new Radians(RecordStats.finalStatsDeflectionAnnAndSixNodes.Max);
                    csvWriter.WriteField(((Degrees)maxAngle).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.WriteField("Average");
                    Radians avgAngle = new Radians(RecordStats.finalStatsDeflectionSixNodesAndHtc.Average);
                    csvWriter.WriteField(((Degrees)avgAngle).ToString("f3"));
                    avgAngle = new Radians(RecordStats.finalStatsDeflectionAnnAndSixNodes.Average);
                    csvWriter.WriteField(((Degrees)avgAngle).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.WriteField("Median");
                    Radians medAngle = new Radians(RecordStats.finalStatsDeflectionSixNodesAndHtc.Median);
                    csvWriter.WriteField(((Degrees)medAngle).ToString("f3"));
                    medAngle = new Radians(RecordStats.finalStatsDeflectionAnnAndSixNodes.Median);
                    csvWriter.WriteField(((Degrees)medAngle).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.WriteField("Percentil 99%");
                    Radians prcntAngle = new Radians(RecordStats.finalStatsDeflectionSixNodesAndHtc.Percentil);
                    csvWriter.WriteField(((Degrees)prcntAngle).ToString("f3"));
                    prcntAngle = new Radians(RecordStats.finalStatsDeflectionAnnAndSixNodes.Percentil);
                    csvWriter.WriteField(((Degrees)prcntAngle).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.NextRecord();

                    csvWriter.WriteField("Метрика  Deflection Distance -- среднее расстояние между " +
                        "концами костей Lower Arm Finch (Six Nodes и ANN) и HTC в см");
                    csvWriter.NextRecord();
                    csvWriter.WriteField("Движение");
                    csvWriter.WriteField("Среднее между Finch Six Nodes и HTC");
                    csvWriter.WriteField("Среднее между Finch ANN и Finch Six Nodes");
                    csvWriter.NextRecord();

                    for (int i = 0; i < RecordStats.distanceLowerArmSixNodesAndHtc.Count; i++)
                    {
                        csvWriter.WriteField(DataManager_v06.FileDatas[i].name);
                        csvWriter.WriteField(((Centimeters)RecordStats.distanceLowerArmSixNodesAndHtc[i].Average).ToString("f3"));
                        csvWriter.WriteField(((Centimeters)RecordStats.distanceLowerArmAnnAndSixNodes[i].Average).ToString("f3"));
                        csvWriter.NextRecord();
                    }

                    csvWriter.NextRecord();

                    csvWriter.WriteField("Min");
                    min = new Meters(RecordStats.finalStatsDistanceLowerArmSixNodesAndHtc.Min);
                    csvWriter.WriteField(((Centimeters)min).ToString("f3"));
                    min = new Meters(RecordStats.finalStatsDistanceLowerArmAnnAndSixNodes.Min);
                    csvWriter.WriteField(((Centimeters)min).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.WriteField("Max");
                    max = new Meters(RecordStats.finalStatsDistanceLowerArmSixNodesAndHtc.Max);
                    csvWriter.WriteField(((Centimeters)max).ToString("f3"));
                    max = new Meters(RecordStats.finalStatsDistanceLowerArmAnnAndSixNodes.Max);
                    csvWriter.WriteField(((Centimeters)max).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.WriteField("Average");
                    avg = new Meters(RecordStats.finalStatsDistanceLowerArmSixNodesAndHtc.Average);
                    csvWriter.WriteField(((Centimeters)avg).ToString("f3"));
                    avg = new Meters(RecordStats.finalStatsDistanceLowerArmAnnAndSixNodes.Average);
                    csvWriter.WriteField(((Centimeters)avg).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.WriteField("Median");
                    med = new Meters(RecordStats.finalStatsDistanceLowerArmSixNodesAndHtc.Median);
                    csvWriter.WriteField(((Centimeters)med).ToString("f3"));
                    med = new Meters(RecordStats.finalStatsDistanceLowerArmAnnAndSixNodes.Median);
                    csvWriter.WriteField(((Centimeters)med).ToString("f3"));
                    csvWriter.NextRecord();

                    csvWriter.WriteField("Percentil 99%");
                    prcnt = new Meters(RecordStats.finalStatsDistanceLowerArmSixNodesAndHtc.Percentil);
                    csvWriter.WriteField(((Centimeters)prcnt).ToString("f3"));
                    prcnt = new Meters(RecordStats.finalStatsDistanceLowerArmAnnAndSixNodes.Percentil);
                    csvWriter.WriteField(((Centimeters)prcnt).ToString("f3"));
                    csvWriter.NextRecord();
                }
            }
        }

        public static void WriteDistance(List<ControllersDistanceError> ToHtcErrors, 
            List<ControllersDistanceError> ToSixNodesErrors, List<float> times)
        {
            using (StreamWriter sw = new StreamWriter(distSixNodesHtcPath, false, Encoding.Default))
            {
                for (int i = 0; i < ToHtcErrors.Count; i++)
                {
                    sw.WriteLine("\t" + times[i] + " " + ((Centimeters)ToHtcErrors[i].rightError).ToString("f3"));
                }
            }

            using (StreamWriter sw = new StreamWriter(distAnnSixNodesPath, false, Encoding.Default))
            {
                for (int i = 0; i < ToSixNodesErrors.Count; i++)
                {
                    sw.WriteLine("\t" + times[i] + " " + ((Centimeters)ToSixNodesErrors[i].rightError).ToString("f3"));
                }
            }
        }

        public static void WriteDeflection(List<LowerArmDirectionsAngleError> ToHtcErrors,
            List<LowerArmDirectionsAngleError> ToSixNodesErrors, List<float> times)
        {
            using (StreamWriter sw = new StreamWriter(deflSixNodesHtcPath, false, Encoding.Default))
            {
                for (int i = 0; i < ToHtcErrors.Count; i++)
                {
                    sw.WriteLine("\t" + times[i] + " " + ((Degrees)ToHtcErrors[i].rightError).ToString("f3"));
                }
            }

            using (StreamWriter sw = new StreamWriter(deflAnnSixNodesPath, false, Encoding.Default))
            {
                for (int i = 0; i < ToSixNodesErrors.Count; i++)
                {
                    sw.WriteLine("\t" + times[i] + " " + ((Degrees)ToSixNodesErrors[i].rightError).ToString("f3"));
                }
            }
        }
    }
}
