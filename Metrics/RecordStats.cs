﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CsvHelper;
using StrongTypes;
using MectricsCSharp;
using g3;

namespace TrackingEvaluation
{
    public class FinalStats
    {
        public List<float> Values;
        public float Average;
        public float Min;
        public float Max;
        public float Median;
        public float Percentil;
    }

    public static class RecordStats
    {
        public static List<Statistics<Meters>> distanceSixNodesAndHtc = new List<Statistics<Meters>>();
        public static FinalStats finalStatsDistanceSixNodesAndHtc = new FinalStats
        {
            Values = new List<float>()
        };
        public static List<Statistics<Meters>> distanceAnnAndSixNodes = new List<Statistics<Meters>>();
        public static FinalStats finalStatsDistanceAnnAndSixNodes = new FinalStats
        {
            Values = new List<float>()
        };
        public static List<Statistics<Meters>> distanceLowerArmSixNodesAndHtc = new List<Statistics<Meters>>();
        public static FinalStats finalStatsDistanceLowerArmSixNodesAndHtc = new FinalStats
        {
            Values = new List<float>()
        };
        public static List<Statistics<Meters>> distanceLowerArmAnnAndSixNodes = new List<Statistics<Meters>>();
        public static FinalStats finalStatsDistanceLowerArmAnnAndSixNodes = new FinalStats
        {
            Values = new List<float>()
        };
        public static List<Statistics<Radians>> deflectionsSixNodesAndHtc = new List<Statistics<Radians>>();
        public static FinalStats finalStatsDeflectionSixNodesAndHtc = new FinalStats
        {
            Values = new List<float>()
        };
        public static List<Statistics<Radians>> deflectionsAnnAndSixNodes = new List<Statistics<Radians>>();
        public static FinalStats finalStatsDeflectionAnnAndSixNodes = new FinalStats
        {
            Values = new List<float>()
        };

        public static void FillStatistics(List<FileData> datas)
        {
            foreach (var data in datas)
            {
                ControllerDistanceMetric controllerDistanceMetric = new ControllerDistanceMetric();

                controllerDistanceMetric.CalculateStatistics(data.controllersSixNodesAndHtcData);
                distanceSixNodesAndHtc.Add(controllerDistanceMetric.rightArmStatistic);
                ControllersDistanceError debugError = controllerDistanceMetric.getAnswer(data.controllersSixNodesAndHtcData[0]);
                Console.WriteLine(debugError.rightError);
                WriterToFiles.WriteDistance(controllerDistanceMetric.getAnswersList(data.controllersSixNodesAndHtcData),
                    controllerDistanceMetric.getAnswersList(data.controllersAnnAndSixNodesData), data.times);
                finalStatsDistanceSixNodesAndHtc.Values.Add(controllerDistanceMetric.rightArmStatistic.Average.Value);

                controllerDistanceMetric.CalculateStatistics(data.controllersAnnAndSixNodesData);
                distanceAnnAndSixNodes.Add(controllerDistanceMetric.rightArmStatistic);
                debugError = controllerDistanceMetric.getAnswer(data.controllersAnnAndSixNodesData[0]);
                Console.WriteLine(debugError.rightError);
                finalStatsDistanceAnnAndSixNodes.Values.Add(controllerDistanceMetric.rightArmStatistic.Average.Value);

                LowerArmDistanceMetric lowerArmDistanceMetric = new LowerArmDistanceMetric();

                lowerArmDistanceMetric.CalculateStatistics(data.lowerArmsDistanceSixNodesAndHtcData);
                distanceLowerArmSixNodesAndHtc.Add(lowerArmDistanceMetric.rightArmStatistic);
                finalStatsDistanceLowerArmSixNodesAndHtc.Values.Add(lowerArmDistanceMetric.rightArmStatistic.Average.Value);

                lowerArmDistanceMetric.CalculateStatistics(data.lowerArmsDistanceAnnAndSixNodes);
                distanceLowerArmAnnAndSixNodes.Add(lowerArmDistanceMetric.rightArmStatistic);
                finalStatsDistanceLowerArmAnnAndSixNodes.Values.Add(lowerArmDistanceMetric.rightArmStatistic.Average.Value);

                LowerArmDirectionsAngleMetric lowerArmDirectionsAngleError = new LowerArmDirectionsAngleMetric();

                lowerArmDirectionsAngleError.CalculateStatistics(data.lowerArmsSixNodesAndHtcData);
                deflectionsSixNodesAndHtc.Add(lowerArmDirectionsAngleError.rightArmStatistic);
                finalStatsDeflectionSixNodesAndHtc.Values.Add(lowerArmDirectionsAngleError.rightArmStatistic.Average.Value);

                WriterToFiles.WriteDeflection(lowerArmDirectionsAngleError.getAnswersList(data.lowerArmsSixNodesAndHtcData),
                    lowerArmDirectionsAngleError.getAnswersList(data.lowerArmsAnnAndSixNodesData), data.times);

                lowerArmDirectionsAngleError.CalculateStatistics(data.lowerArmsAnnAndSixNodesData);
                deflectionsAnnAndSixNodes.Add(lowerArmDirectionsAngleError.rightArmStatistic);
                finalStatsDeflectionAnnAndSixNodes.Values.Add(lowerArmDirectionsAngleError.rightArmStatistic.Average.Value);
            }

            CalculateFinalStats();
        }

        private static void CalculateFinalStats()
        {
            finalStatsDistanceSixNodesAndHtc.Average = StatsHelper.GetAverage(finalStatsDistanceSixNodesAndHtc.Values);
            finalStatsDistanceSixNodesAndHtc.Max = StatsHelper.GetMax(finalStatsDistanceSixNodesAndHtc.Values);
            finalStatsDistanceSixNodesAndHtc.Min = StatsHelper.GetMin(finalStatsDistanceSixNodesAndHtc.Values);
            OrderedValues sorted = StatsHelper.Sort(finalStatsDistanceSixNodesAndHtc.Values);
            finalStatsDistanceSixNodesAndHtc.Median = StatsHelper.GetMedian(sorted);
            finalStatsDistanceSixNodesAndHtc.Percentil = StatsHelper.GetPercentil(sorted, 99);

            finalStatsDistanceAnnAndSixNodes.Average = StatsHelper.GetAverage(finalStatsDistanceAnnAndSixNodes.Values);
            finalStatsDistanceAnnAndSixNodes.Max = StatsHelper.GetMax(finalStatsDistanceAnnAndSixNodes.Values);
            finalStatsDistanceAnnAndSixNodes.Min = StatsHelper.GetMin(finalStatsDistanceAnnAndSixNodes.Values);
            sorted = StatsHelper.Sort(finalStatsDistanceAnnAndSixNodes.Values);
            finalStatsDistanceAnnAndSixNodes.Median = StatsHelper.GetMedian(sorted);
            finalStatsDistanceAnnAndSixNodes.Percentil = StatsHelper.GetPercentil(sorted, 99);

            finalStatsDistanceLowerArmSixNodesAndHtc.Average = StatsHelper.GetAverage(finalStatsDistanceLowerArmSixNodesAndHtc.Values);
            finalStatsDistanceLowerArmSixNodesAndHtc.Max = StatsHelper.GetMax(finalStatsDistanceLowerArmSixNodesAndHtc.Values);
            finalStatsDistanceLowerArmSixNodesAndHtc.Min = StatsHelper.GetMin(finalStatsDistanceLowerArmSixNodesAndHtc.Values);
            sorted = StatsHelper.Sort(finalStatsDistanceLowerArmSixNodesAndHtc.Values);
            finalStatsDistanceLowerArmSixNodesAndHtc.Median = StatsHelper.GetMedian(sorted);
            finalStatsDistanceLowerArmSixNodesAndHtc.Percentil = StatsHelper.GetPercentil(sorted, 99);

            finalStatsDistanceLowerArmAnnAndSixNodes.Average = StatsHelper.GetAverage(finalStatsDistanceLowerArmAnnAndSixNodes.Values);
            finalStatsDistanceLowerArmAnnAndSixNodes.Max = StatsHelper.GetMax(finalStatsDistanceLowerArmAnnAndSixNodes.Values);
            finalStatsDistanceLowerArmAnnAndSixNodes.Min = StatsHelper.GetMin(finalStatsDistanceLowerArmAnnAndSixNodes.Values);
            sorted = StatsHelper.Sort(finalStatsDistanceLowerArmAnnAndSixNodes.Values);
            finalStatsDistanceLowerArmAnnAndSixNodes.Median = StatsHelper.GetMedian(sorted);
            finalStatsDistanceLowerArmAnnAndSixNodes.Percentil = StatsHelper.GetPercentil(sorted, 99);

            finalStatsDeflectionSixNodesAndHtc.Average = StatsHelper.GetAverage(finalStatsDeflectionSixNodesAndHtc.Values);
            finalStatsDeflectionSixNodesAndHtc.Max = StatsHelper.GetMax(finalStatsDeflectionSixNodesAndHtc.Values);
            finalStatsDeflectionSixNodesAndHtc.Min = StatsHelper.GetMin(finalStatsDeflectionSixNodesAndHtc.Values);
            sorted = StatsHelper.Sort(finalStatsDeflectionSixNodesAndHtc.Values);
            finalStatsDeflectionSixNodesAndHtc.Median = StatsHelper.GetMedian(sorted);
            finalStatsDeflectionSixNodesAndHtc.Percentil = StatsHelper.GetPercentil(sorted, 99);

            finalStatsDeflectionAnnAndSixNodes.Average = StatsHelper.GetAverage(finalStatsDeflectionAnnAndSixNodes.Values);
            finalStatsDeflectionAnnAndSixNodes.Max = StatsHelper.GetMax(finalStatsDeflectionAnnAndSixNodes.Values);
            finalStatsDeflectionAnnAndSixNodes.Min = StatsHelper.GetMin(finalStatsDeflectionAnnAndSixNodes.Values);
            sorted = StatsHelper.Sort(finalStatsDeflectionAnnAndSixNodes.Values);
            finalStatsDeflectionAnnAndSixNodes.Median = StatsHelper.GetMedian(sorted);
            finalStatsDeflectionAnnAndSixNodes.Percentil = StatsHelper.GetPercentil(sorted, 99);
        }
    }
}
