﻿using System;
using System.Collections.Generic;
using System.IO;
using MectricsCSharp;
using StrongTypes;
using Format_v06;
using Finch;
using g3;

namespace TrackingEvaluation
{
    public static class DataFiller
    {
        public static void FillRecordData(MetaData metaData, List<FrameData> frameDatas, ref FileData fileData)
        {
            FinchCore.Init(new FinchSettings());
            for (int i = 0; i < frameDatas.Count; i++)
            {
                fileData.hmdData = new HmdData
                {
                    HmdRotation = frameDatas[i].HtcHmd.Rotation,
                    HmdPosition = frameDatas[i].HtcHmd.Position
                };

                fileData.packages = new Packages
                {
                    LeftUpperArm = frameDatas[i].FinchLeftUpperArm.Package,
                    LeftLowerArm = frameDatas[i].FinchLeftLowerArm.Package,
                    LeftHand = frameDatas[i].FinchLeftHand.Package,
                    RightUpperArm = frameDatas[i].FinchRightUpperArm.Package,
                    RightLowerArm = frameDatas[i].FinchRightLowerArm.Package,
                    RightHand = frameDatas[i].FinchRightHand.Package
                };

                FinchAnnData annData = FillFinchAnnData(fileData.packages, fileData.hmdData, metaData);

                fileData.times.Add(frameDatas[i].Time);

                ConrollersInfoFrame sixNodesAndHtcDistance = new ConrollersInfoFrame
                {
                    LeftMeasuringControllerPosition = frameDatas[i].FinchPositions.LeftControllerPosition,
                    RightMeasuringControllerPosition = frameDatas[i].FinchPositions.RightControllerPosition,
                    LeftStandardControllerPosition = frameDatas[i].HtcLeftController.ControllerPosition,
                    RightStandardControllerPosition = frameDatas[i].HtcRightController.ControllerPosition
                };
                fileData.controllersSixNodesAndHtcData.Add(sixNodesAndHtcDistance);

                ConrollersInfoFrame annAndSixNodesDistance = new ConrollersInfoFrame
                {
                    LeftMeasuringControllerPosition = annData.LeftControllerPosition,
                    RightMeasuringControllerPosition = annData.RightControllerPostion,
                    LeftStandardControllerPosition = frameDatas[i].FinchPositions.LeftControllerPosition,
                    RightStandardControllerPosition = frameDatas[i].FinchPositions.RightControllerPosition
                };
                fileData.controllersAnnAndSixNodesData.Add(annAndSixNodesDistance);

                LowerArmDistanceInfoFrame lowerArmSixNodesAndHtcDistance = new LowerArmDistanceInfoFrame
                {
                    LeftMeasuringLowerArmRotation = frameDatas[i].FinchLeftLowerArm.Rotation,
                    RightMeasuringLowerArmRotation = frameDatas[i].FinchRightLowerArm.Rotation,
                    LeftStandardLowerArmRotation = frameDatas[i].HtcLeftLowerArm.Rotation,
                    RightStandardLowerArmRotation = frameDatas[i].HtcRightLowerArm.Rotation,
                    LeftParentBone = frameDatas[i].FinchPositions.LeftLowerArmBonePosition,
                    RightParentBone = frameDatas[i].FinchPositions.RightLowerArmBonePosition,
                    LeftParentBoneLength = metaData.UserPreset.LeftLowerArmBoneLength,
                    RightParentBoneLength = metaData.UserPreset.RightLowerArmBoneLength
                    //LeftParentBoneLength = FinchCore.Interop.FinchGetBoneLength(FinchCore.Interop.FinchBone.LeftUpperArm),
                    //RightParentBoneLength = FinchCore.Interop.FinchGetBoneLength(FinchCore.Interop.FinchBone.RightUpperArm)
                };
                fileData.lowerArmsDistanceSixNodesAndHtcData.Add(lowerArmSixNodesAndHtcDistance);

                LowerArmDistanceInfoFrame lowerArmAnnAndSixNodesDistance = new LowerArmDistanceInfoFrame
                {
                    LeftMeasuringLowerArmRotation = annData.LeftLowerArmAnn,
                    RightMeasuringLowerArmRotation = annData.RightLowerArmAnn,
                    LeftStandardLowerArmRotation = frameDatas[i].FinchLeftLowerArm.Rotation,
                    RightStandardLowerArmRotation = frameDatas[i].FinchRightLowerArm.Rotation,
                    LeftParentBone = frameDatas[i].FinchPositions.LeftLowerArmBonePosition,
                    RightParentBone = frameDatas[i].FinchPositions.RightLowerArmBonePosition,
                    LeftParentBoneLength = metaData.UserPreset.LeftLowerArmBoneLength,
                    RightParentBoneLength = metaData.UserPreset.RightLowerArmBoneLength,
                    //LeftParentBoneLength = FinchCore.Interop.FinchGetBoneLength(FinchCore.Interop.FinchBone.LeftUpperArm),
                    //RightParentBoneLength = FinchCore.Interop.FinchGetBoneLength(FinchCore.Interop.FinchBone.RightUpperArm)
                };
                fileData.lowerArmsDistanceAnnAndSixNodes.Add(lowerArmAnnAndSixNodesDistance);

                LowerArmInfoFrame lowerArmAnnAndSixNodesFrame = new LowerArmInfoFrame
                {
                    LeftMeasuringLowerArmRotation = annData.LeftLowerArmAnn,
                    RightMeasuringLowerArmRotation = annData.RightLowerArmAnn,
                    LeftStandardLowerArmRotation = frameDatas[i].FinchLeftLowerArm.Rotation,
                    RightStandardLowerArmRotation = frameDatas[i].FinchRightLowerArm.Rotation,
                };
                fileData.lowerArmsAnnAndSixNodesData.Add(lowerArmAnnAndSixNodesFrame);

                LowerArmInfoFrame lowerArmSixNodesAndHtcFrame = new LowerArmInfoFrame
                {
                    LeftMeasuringLowerArmRotation = frameDatas[i].FinchLeftLowerArm.Rotation,
                    RightMeasuringLowerArmRotation = frameDatas[i].FinchRightLowerArm.Rotation,
                    LeftStandardLowerArmRotation = frameDatas[i].HtcLeftLowerArm.Rotation,
                    RightStandardLowerArmRotation = frameDatas[i].HtcRightLowerArm.Rotation
                };
                fileData.lowerArmsSixNodesAndHtcData.Add(lowerArmSixNodesAndHtcFrame);
            }
        }

        private static FinchAnnData FillFinchAnnData(Packages packages, HmdData hmdData, MetaData meta)
        {
            FinchCore.Interop.FinchSetCalibrationAdjust(FinchCore.Interop.FinchNodeType.LeftHand,
                (Quaternionf)meta.FinchLeftHand.Adjusts.Pre, isPre: 1, useDefaultCS: 0);
            FinchCore.Interop.FinchSetCalibrationAdjust(FinchCore.Interop.FinchNodeType.LeftHand,
                (Quaternionf)meta.FinchLeftHand.Adjusts.Post, isPre: 0, useDefaultCS: 0);

            //FinchCore.Interop.FinchSetCalibrationAdjust(FinchCore.Interop.FinchNodeType.LeftLowerArm,
            //    (Quaternionf)meta.FinchLeftLowerArm.Adjusts.Pre, isPre: 1, useDefaultCS: 0);
            //FinchCore.Interop.FinchSetCalibrationAdjust(FinchCore.Interop.FinchNodeType.LeftLowerArm,
            //    (Quaternionf)meta.FinchLeftLowerArm.Adjusts.Post, isPre: 0, useDefaultCS: 0);

            FinchCore.Interop.FinchSetCalibrationAdjust(FinchCore.Interop.FinchNodeType.LeftUpperArm,
                (Quaternionf)meta.FinchLeftUpperArm.Adjusts.Pre, isPre: 1, useDefaultCS: 0);
            FinchCore.Interop.FinchSetCalibrationAdjust(FinchCore.Interop.FinchNodeType.LeftUpperArm,
                (Quaternionf)meta.FinchLeftUpperArm.Adjusts.Post, isPre: 0, useDefaultCS: 0);

            FinchCore.Interop.FinchSetCalibrationAdjust(FinchCore.Interop.FinchNodeType.RightHand,
                (Quaternionf)meta.FinchRightHand.Adjusts.Pre, isPre: 1, useDefaultCS: 0);
            FinchCore.Interop.FinchSetCalibrationAdjust(FinchCore.Interop.FinchNodeType.RightHand,
                (Quaternionf)meta.FinchRightHand.Adjusts.Post, isPre: 0, useDefaultCS: 0);

            //FinchCore.Interop.FinchSetCalibrationAdjust(FinchCore.Interop.FinchNodeType.RightLowerArm,
            //    (Quaternionf)meta.FinchRightLowerArm.Adjusts.Pre, isPre: 1, useDefaultCS: 0);
            //FinchCore.Interop.FinchSetCalibrationAdjust(FinchCore.Interop.FinchNodeType.RightLowerArm,
            //    (Quaternionf)meta.FinchRightLowerArm.Adjusts.Post, isPre: 0, useDefaultCS: 0);

            FinchCore.Interop.FinchSetCalibrationAdjust(FinchCore.Interop.FinchNodeType.RightUpperArm,
                (Quaternionf)meta.FinchRightUpperArm.Adjusts.Pre, isPre: 1, useDefaultCS: 0);
            FinchCore.Interop.FinchSetCalibrationAdjust(FinchCore.Interop.FinchNodeType.RightUpperArm,
                (Quaternionf)meta.FinchRightUpperArm.Adjusts.Post, isPre: 0, useDefaultCS: 0);

            FinchCore.Interop.FinchApply();

            FinchCore.Interop.FinchExternHmdTransformUpdate(packages.RightHand, packages.LeftHand,
                packages.RightUpperArm, packages.LeftUpperArm,
                hmdData.HmdRotation, hmdData.HmdPosition);
            FinchCore.Interop.FinchExternHmdTransformUpdate(packages.RightHand, packages.LeftHand,
                packages.RightUpperArm, packages.LeftUpperArm,
                hmdData.HmdRotation, hmdData.HmdPosition);

            return new FinchAnnData
            {
                LeftLowerArmAnn = FinchCore.Interop.FinchGetBoneRotation(FinchCore.Interop.FinchBone.LeftLowerArm, fPose: 0),
                RightLowerArmAnn = FinchCore.Interop.FinchGetBoneRotation(FinchCore.Interop.FinchBone.RightLowerArm, fPose: 0),
                LeftControllerPosition = FinchCore.Interop.FinchGetControllerPosition(FinchCore.Interop.FinchChirality.Left, 1),
                RightControllerPostion = FinchCore.Interop.FinchGetControllerPosition(FinchCore.Interop.FinchChirality.Right, 1)
            };
        }
    }
}
