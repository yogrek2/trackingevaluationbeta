﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackingEvaluation
{
    class Program
    {
        static void Main(string[] args)
        {
            DataManager_v06.ReadDataFromDB();
            DataManager_v06.CalculateMetrics();
            
            WriterToFiles.WriteStatsToCsv();

            Console.WriteLine("Done!");

            Console.ReadKey();
        }
    }
}
