﻿using System;
using System.Collections.Generic;
using System.IO;
using MectricsCSharp;
using StrongTypes;
using Format_v06;
using g3;

namespace TrackingEvaluation
{
    public class Packages
    {
        public byte[] LeftUpperArm;
        public byte[] LeftLowerArm;
        public byte[] LeftHand;
        public byte[] RightUpperArm;
        public byte[] RightLowerArm;
        public byte[] RightHand;
    }

    public class HmdData
    {
        public Quaternionf HmdRotation;
        public Vector3f HmdPosition;
    }

    public class FinchAnnData
    {
        public Quaternionf LeftLowerArmAnn;
        public Quaternionf RightLowerArmAnn;
        public Vector3f LeftControllerPosition;
        public Vector3f RightControllerPostion;
    }

    public class FileData
    {
        public string name;
        public List<float> times;
        public List<ConrollersInfoFrame> controllersSixNodesAndHtcData;
        public List<ConrollersInfoFrame> controllersAnnAndSixNodesData;
        public List<LowerArmDistanceInfoFrame> lowerArmsDistanceSixNodesAndHtcData;
        public List<LowerArmDistanceInfoFrame> lowerArmsDistanceAnnAndSixNodes;
        public List<LowerArmInfoFrame> lowerArmsAnnAndSixNodesData;
        public List<LowerArmInfoFrame> lowerArmsSixNodesAndHtcData;
        public HmdData hmdData;
        public Packages packages;
    }

    public class DataManager_v06
    {
        public static List<FileData> FileDatas = new List<FileData>();
        public static string directory = Directory.GetCurrentDirectory();
        public static string dbpath = Directory.GetParent(Directory.GetParent(directory).Parent.FullName) + @"\DataBases";
        private static List<string> Records = new List<string>();

        public static void ReadDataFromDB()
        {
            if (!Directory.Exists(dbpath))
                Directory.CreateDirectory(dbpath);

            foreach (var i in Directory.GetFiles(dbpath))
            {
                if (i.Contains(".RD06"))
                    Records.Add(i);
            }

            foreach(var record in Records)
            {
                SQLiteMapper_v06 wrtr = new SQLiteMapper_v06();
                Console.WriteLine("Start reading from database");
                RecordData RecOut = wrtr.Read(dbpath, Path.GetFileNameWithoutExtension(record));
                Console.WriteLine("End reading from database");
                List<FrameData> dbData = RecOut.FrameData;
                MetaData mData = RecOut.MetaData;
                FileData fileData = new FileData()
                {
                    name = Path.GetFileNameWithoutExtension(record),
                    times = new List<float>(),
                    controllersSixNodesAndHtcData = new List<ConrollersInfoFrame>(),
                    controllersAnnAndSixNodesData = new List<ConrollersInfoFrame>(),
                    lowerArmsDistanceSixNodesAndHtcData = new List<LowerArmDistanceInfoFrame>(),
                    lowerArmsDistanceAnnAndSixNodes = new List<LowerArmDistanceInfoFrame>(),
                    lowerArmsAnnAndSixNodesData = new List<LowerArmInfoFrame>(),
                    lowerArmsSixNodesAndHtcData = new List<LowerArmInfoFrame>()
                };
                DataFiller.FillRecordData(mData, dbData, fileData: ref fileData);
                FileDatas.Add(fileData);
                dbData.Clear();
            }
        }

        public static void CalculateMetrics()
        {
            Console.WriteLine("Start Calculataing Metrics");
            RecordStats.FillStatistics(FileDatas);
            Console.WriteLine("End Calculataing Metrics");
        }
    }
}
